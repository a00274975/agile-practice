
import java.util.Scanner;
 
public class conversion_1
{
  public static double convert(double amount)
  {
    double euro = amount / 1.18;
    return euro;
  }
  public static void main(String[] args)
  {
    Scanner input = new Scanner(System.in);
    System.out.println("Enter amount $: ");
    double amount = input.nextDouble();
    System.out.println("In Euro: ");
    System.out.print(convert(amount));
    input.close();
  }
}
